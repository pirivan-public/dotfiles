alias enw="emacs -nw"
alias lg=lazygit
alias k=kubectl
alias ls='exa'
alias pip='python -m pip'
alias mtr='/usr/local/sbin/mtr'
alias gc-="git checkout -"

# editor
export EDITOR="micro"
export MICRO_TRUECOLOR=1

# golang
GOPROXY=direct

# paths
#export GOPATH="$HOME/Workspace/go"
#if [[ "$OSTYPE" == "darwin"* ]]; then
#	export GOPATH=$HOME
#fi
#export PATH=$GOPATH/bin:$PATH
export PATH="$HOME/bin:$HOME/.local/bin:$HOME/.poetry/bin:$PATH"
