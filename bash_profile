alias enw="emacs -nw"
alias lg=lazygit
alias k=kubectl
alias pip='python -m pip'

# editor
export EDITOR="micro"
export MICRO_TRUECOLOR=1

# paths
export GOPATH="$HOME/Workspace/go"
if [[ "$OSTYPE" == "darwin"* ]]; then
	export GOPATH=$HOME
fi
export PATH=$GOPATH/bin:$PATH
export PATH="$HOME/.local/bin:$HOME/.poetry/bin:$PATH"

# pyenv
if command -v pyenv 1>/dev/null 2>&1; then eval "$(pyenv init -)"; fi

# MacOS
if [[ "$OSTYPE" == "darwin"* ]]; then
	# Shopify dev environment
	if [[ -f /opt/dev/dev.sh ]]; then source /opt/dev/dev.sh; fi
	if [ -e /Users/psanchez/.nix-profile/etc/profile.d/nix.sh ]; then . /Users/psanchez/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
fi
