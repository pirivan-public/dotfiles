;; https://github.com/magnars/.emacs.d.git
;; ... and many others!

;; set up packages
(setq my-packages
      '(auctex
        ein
        elpy
        exec-path-from-shell
        fill-column-indicator
        flycheck
        go-mode
        ido-completing-read+
        jedi
        lua-mode
        magit
        markdown-mode
        markdown-toc
        material-theme
        no-littering
        org
        plantuml-mode
        py-autopep8
        smartparens
        toml-mode
        tramp
        yaml-mode
        ))

(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

;; Set up customization file
(setq custom-file
      (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)

;; Set up path to local settings
(setq settings-dir
      (expand-file-name "settings" user-emacs-directory))
(add-to-list 'load-path settings-dir)

;; Load settings
(require 'setup-no-littering)
(require 'sane-defaults)
(require 'key-bindings)
(require 'setup-ido)
(require 'setup-org)
(require 'setup-plantuml)
(require 'setup-python-mode)
(require 'setup-golang)
(eval-after-load 'markdown-mode '(require 'setup-markdown-mode))
