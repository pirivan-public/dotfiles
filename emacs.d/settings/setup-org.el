;; http://blog.aaronbieber.com/2016/01/30/dig-into-org-mode.html
;; http://doc.norang.ca/org-mode.html
;; (to review: http://juanreyero.com/article/emacs/org-teams.html)
;; (to review: http://orgmode.org/worg/org-tutorials/org-meeting-tasks.html)


;; personal information used in for export and publish operations
(setq user-full-name "Pedro I. Sánchez"
      user-mail-address "pedro.sanchez@fosstel.com"
      org-export-default-language "en")

;; global key bindings
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)
(global-set-key "\C-cl" 'org-store-link)

;; general options
(setq org-startup-indented t)
(setq org-export-coding-system 'utf-8)
(setq org-completion-use-ido t)
(setq org-hide-emphasis-markers t)
(setq org-pretty-entities t)
(setq org-clock-into-drawer t)
(setq org-export-with-smart-quotes t)

;; org root directory
(setq org-directory "~/Documents/Org/")

;; file structure
(let ((default-directory org-directory))
  (setq my-journal-file (expand-file-name "journal.org"))
  (setq my-notes-file (expand-file-name "notes.org"))
  (setq my-org-agenda-path (expand-file-name "agendas/")))

;; agenda
(setq org-directory default-directory
      org-agenda-files (list my-org-agenda-path my-notes-file)
      org-agenda-text-search-extra-files (list my-journal-file))
(load "setup-org-agenda-views.el")

;; refiling
(setq org-refile-targets '((org-agenda-files :maxlevel . 1))
      org-refile-use-outline-path 'file
      org-outline-path-complete-in-steps nil
      org-refile-allow-creating-parent-nodes 'confirm)

;; TODO items
(setq org-todo-keywords
      '((sequence "TODO(t!)" "IN-PROGRESS(i@)" "ON-HOLD(h@/!)" "|" "DONE(d!)" "CANCELLED(c@)")))
(setq org-enforce-todo-dependencies t
      org-use-fast-todo-selection t
      org-log-into-drawer t
      org-log-done 'time
      org-log-redeadline 'time
      org-log-reschedule 'time
      org-treat-insert-todo-heading-as-state-change t)
(setq org-todo-keyword-faces
      '(("TODO"        . (:foreground "wheat2"))
        ("IN-PROGRESS" . (:foreground "MediumSeaGreen" :weight bold))
        ("ON-HOLD"     . (:foreground "firebrick" :weight bold))
        ("DONE"        . (:foreground "DarkOliveGreen4"))
        ("CANCELLED"   . (:foreground "gray50"))))
(setq org-priority-faces
      '((?A . (:foreground "SkyBlue1" :weight bold))
        (?B . (:foreground "SkyBlue2"))
        (?C . (:foreground "SkyBlue4"))))

;; Tags
(setq org-fast-tag-selection-single-key t
      org-tag-alist '((:startgroup . nil)
                      ("draft" . ?d)
                      ("final" . ?f)
                      (:endgroup . nil)))

;; Org capture
(setq org-default-notes-file my-notes-file)
(setq org-capture-templates
      (quote
       (("j" "journal"
         entry (file+datetree my-journal-file)
         "* %U %^{Title}\n%?"
         :empty-lines 1)
        ("n" "note"
         entry (file+headline my-notes-file "Notes")
         "* %^{Title} :draft:\nCreated: %U\n\n%?"
         :empty-lines 1)
        ("t" "todo"
         entry (file+headline my-notes-file "Tasks")
         "* TODO [#B] %^{Task}\nCreated: %U\n\n%?"
         :empty-lines 1)
        )))

(provide 'setup-org)
